package pszt;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import pszt.model.Model;
import pszt.controller.Controller;
import pszt.view.View;
import pszt.events.AppEvent;

public class Main {

	/**
	 * Uruchamia aplikacje.
	 * @param args
	 */
    public static void main(String[] args) {
    	
    	// Kolejka blokująca dla widoku i kontrolera.
		final BlockingQueue<AppEvent> eventsBlockingQueue = new LinkedBlockingQueue<AppEvent>();
		// model
		final Model model = new Model(eventsBlockingQueue);
		// widok
		final View view = new View(eventsBlockingQueue);
		// kontroler
		final Controller controller = new Controller(model, view, eventsBlockingQueue);
		// Uruchomienie.
		controller.init();
    }
}
