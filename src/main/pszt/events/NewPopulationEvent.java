package pszt.events;

import pszt.mockups.PopulationMockup;

public class NewPopulationEvent extends AppEvent {

    private PopulationMockup populationMockup;

    public NewPopulationEvent(PopulationMockup populationMockup) {
        this.populationMockup = populationMockup;
    }

    public PopulationMockup getPopulationMockup() {
        return populationMockup;
    }
}
