package pszt.events;

public class ShowChosenPopulationEvent extends AppEvent
{
    final int populationNumber;

    public ShowChosenPopulationEvent(int populationNumber)
    {
        this.populationNumber = populationNumber;
    }

    public int getPopulationNumber() {
        return populationNumber;
    }
}
