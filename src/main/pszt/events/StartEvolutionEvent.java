package pszt.events;

import pszt.mockups.EvolutionParametersMockup;
import pszt.mockups.SettingsMockup;

/**
 * 
 * Wydarzenie, które powstaje po kliknięciu przez użytkownika przycisku "Start" i
 * uruchomieniu liczenia minimum funkcji.
 * 
 * Zawiera parametry dotyczące ewolucji (w tym parametry populacji).
 */
public class StartEvolutionEvent extends AppEvent
{
	/** Ustawienia ewolucji */
	private final EvolutionParametersMockup	evolutionParametersMockup;
	/** Inne ustawienia */
	private final SettingsMockup			settingsMockup;

	public StartEvolutionEvent(final EvolutionParametersMockup evolutionParametersMockup,
			final SettingsMockup settingsMockup)
	{
		this.evolutionParametersMockup = evolutionParametersMockup;
		this.settingsMockup = settingsMockup;
	}

	public EvolutionParametersMockup getEvolutionParametersMockup()
	{
		return evolutionParametersMockup;
	}

	public SettingsMockup getSettingsMockup()
	{
		return settingsMockup;
	}

}
