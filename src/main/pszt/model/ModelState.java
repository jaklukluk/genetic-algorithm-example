package pszt.model;

public enum ModelState
{
	EVOLUTION_RUNS,
	EVOLUTION_STOPPED,
	EVOLUTION_PAUSED,
	EVOLUTION_ENDED;
}
