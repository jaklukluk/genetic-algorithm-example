/**
 * 
 */
package pszt.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;

import pszt.events.AppEvent;
import pszt.mockups.EvolutionParametersMockup;
import pszt.mockups.IndividualMockup;
import pszt.mockups.PopulationMockup;

/**
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class Model
{
	/** Aktualny stan ewolucji. */
	private ModelState							modelState;
	/** Kolejka blokująca zdarzeń. */
	private final BlockingQueue<AppEvent>		eventsBlockingQueue;
	/** Prawda, jeżeli rysujemy wykresy; Fałsz wpp. */
	private boolean								drawCharts;

	/** Obiekt ewolucji. */
	private Evolution							evolution;
	/** Wątek ewolucji. */
	private Thread								evoThread;
	/** Lista minionych populacji. */
	private final ArrayList<PopulationMockup>	oldPopulations;
	/** Lista najlepszych osobników z minionych populacji. */
	private final LinkedList<IndividualMockup>	bestIndividuals;

	public Model(BlockingQueue<AppEvent> eventsBlockingQueue)
	{
		this.modelState = ModelState.EVOLUTION_STOPPED;
		this.evolution = null;
		this.eventsBlockingQueue = eventsBlockingQueue;
		this.drawCharts = true;
		this.bestIndividuals = new LinkedList<>();
		this.oldPopulations = new ArrayList<>();
	}

	public void startEvolution(EvolutionParametersMockup evoParams)
	{
		modelState = ModelState.EVOLUTION_RUNS;
		oldPopulations.clear();
		bestIndividuals.clear();
		evolution = new Evolution(evoParams, eventsBlockingQueue);
		evoThread = new Thread(evolution);
		evoThread.start();
	}

	public void pauseEvolution()
	{
		modelState = ModelState.EVOLUTION_PAUSED;
		evolution.pause();
	}

	public void unpauseEvolution()
	{
		modelState = ModelState.EVOLUTION_RUNS;
		evolution.unpause();
	}

	public void stopEvolution()
	{
		modelState = ModelState.EVOLUTION_STOPPED;
		evolution.stop();
	}

	public boolean hasEvolutionStarted()
	{
		return (modelState == ModelState.EVOLUTION_RUNS || modelState == ModelState.EVOLUTION_PAUSED);
	}

	public boolean evolutionRuns()
	{
		return modelState == ModelState.EVOLUTION_RUNS;
	}

	public ModelState getModelState()
	{
		return modelState;
	}

	public void setModelState(ModelState modelState)
	{
		this.modelState = modelState;
	}

	public boolean areChartsEnable()
	{
		return drawCharts;
	}

	public void enableCharts(boolean drawCharts)
	{
		this.drawCharts = drawCharts;
	}

	public PopulationMockup getOldPopulation(int index)
	{
		return oldPopulations.get(index);
	}

    public int getOldPopulationsSize()
    {
        return oldPopulations.size();
    }

    public PopulationMockup getLastPopulation()
    {
        return oldPopulations.get(getOldPopulationsSize() - 1);
    }

    public int getLastPopulationIndex()
    {
        return oldPopulations.size() - 1;
    }
	
	public void addPopulation(PopulationMockup populationMockup)
	{
		oldPopulations.add(populationMockup);
		bestIndividuals.add(populationMockup.getBest());
	}

	public LinkedList<IndividualMockup> getBestIndividuals()
	{
		return bestIndividuals;
	}
}
