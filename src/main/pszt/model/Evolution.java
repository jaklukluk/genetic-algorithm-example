package pszt.model;

import java.util.concurrent.BlockingQueue;
import pszt.events.AppEvent;
import pszt.events.EvolutionEndedEvent;
import pszt.events.NewPopulationEvent;
import pszt.mockups.EvolutionParametersMockup;
import pszt.mockups.PopulationMockup;
import pszt.mockups.PopulationParametersMockup;
import pszt.model.ga.Population;
import pszt.common.Range;

public class Evolution implements Runnable
{
	/** Populacja. */
	private final Population				population;
	/** Maksymalna ilość przebiegów ewolucji */
	private final int						maxIterations;
	/** Wystarczająca odległość od zera. */
	private final double					sufficientFitness;	
	/** Okres powtarzania w ms. */
	private final int						refreshTime;	
	/** Kolejka blokująca zdarzeń. */
	private final BlockingQueue<AppEvent>	eventsBlockingQueue;
	/** Prawda, jeżeli ewolucja została wstrzymana; Fałsz wpp. */
	private volatile boolean				evoPaused;
	/** Prawda, jeżeli ewolucja została zastopowana; Fałsz wpp. */
	private volatile boolean				evoStopped;

	public Evolution(final EvolutionParametersMockup evoParams,
			final BlockingQueue<AppEvent> eventsBlockingQueue)
	{
		this.eventsBlockingQueue = eventsBlockingQueue;

		this.maxIterations = evoParams.getMaxIterations();
		this.sufficientFitness = evoParams.getSufficientFitness();
		this.refreshTime = evoParams.getRefreshTime();
		this.evoPaused = false;

		PopulationParametersMockup popParams = evoParams.getPopulationParameters();

		int popSize = popParams.getPopulationSize();
		float crossoverRatio = popParams.getCrossoverRatio();
		float elitismRatio = popParams.getElitismRatio();
		float mutationRatio = popParams.getMutationRatio();
		Range range = new Range(popParams.getMinRange(), popParams.getMaxRange());
		this.population = new Population(popSize, crossoverRatio, elitismRatio,
				mutationRatio, range);
	}

	/**
	 * Tutaj przebiega ewolucja populacji w celu znalezienia najlepszego osobnika.
	 */
	@Override
	public void run()
	{
		// Ewolucja będzie biec dopóki
		// nie zostanie zatrzymana (evoStopped), nie przekroczyliśmy maksymalnej liczby
		// iteracji, lub nie znajdziemy wystarczająco dobrego osobnika.
		int iterations = 0;
		while (iterations <= maxIterations && !evoStopped
				&& population.getPopulation()[0].getFitness() - sufficientFitness > 0)
		{
			try
			{
				++iterations;

				population.evolve();
				eventsBlockingQueue.put(buildNewPopulationEvent());

				Thread.sleep(refreshTime);

				// Sprawdzenie, czy wątek należy wstrzymać.
				if (evoPaused)
				{
					synchronized (this)
					{
						while (evoPaused)
						{
							wait();
						}
					}
				}
			} catch (InterruptedException e)
			{

			}
		}

		evolutionEnded(iterations);
	}

	synchronized public void pause()
	{
		evoPaused = true;
	}

	synchronized public void unpause()
	{
		evoPaused = false;
		notifyAll();
	}

	synchronized public void stop()
	{
		evoStopped = true;
		notifyAll();
	}

	private NewPopulationEvent buildNewPopulationEvent()
	{
		PopulationMockup populationMockup = population.getMockup();
		return new NewPopulationEvent(populationMockup);
	}

	private void evolutionEnded(int iterations)
	{
		try
		{
			eventsBlockingQueue.put(new EvolutionEndedEvent());
		} catch (InterruptedException e)
		{

		}
	}

}
