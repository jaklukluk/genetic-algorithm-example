package pszt.model.ga;

import java.util.Arrays;
import java.util.Random;

/**
 * Klasa opisująca chromosom.
 *
 * Wywołanie dowolnej metody nie zmienia zawartości obiektu.
 */
public class Chromosome {

    private static Random rnd = new Random();

    private boolean[] genotype;

    public Chromosome(boolean[] genotype) {
        this.genotype = genotype;
    }

    public Chromosome(Chromosome chromosome) {
        this(chromosome.getGenotype());
    }

    /**
     * Zwraca kopię genotypu.
     */
    public boolean[] getGenotype() {
        boolean[] arr = new boolean[genotype.length];
        System.arraycopy(genotype, 0, arr, 0, genotype.length);
        return arr;
    }

    /**
     * Neguje bity genotypu na podstawie zadanej wartości prawdopodobieństwa.
     */
    public Chromosome mutate(float mutationRatio) {
        boolean[] mutatedGenotype = getGenotype();
        for (int i = 0; i < mutatedGenotype.length; ++i) {
            if (rnd.nextFloat() < mutationRatio) {
                mutatedGenotype[i] = !mutatedGenotype[i];
            }
        }
        return new Chromosome(mutatedGenotype);
    }

    /**
     * Zamienia bity genotypu na liczbę rzeczywistą z podanego zakresu.
     * Genotyp jest zakodowany w kodzie graya.
     * @param min minimalna liczba
     * @param max maksymalna liczba
     */
    public double getValue(double min, double max) {
        boolean prevBit = genotype[0];
        int result = prevBit ? 1 : 0;
        for (int i = 1; i < genotype.length; ++i) {
            prevBit = genotype[i] ^ prevBit;
            result = (prevBit ? 1 : 0) + result * 2;
        }
        return min + (max - min) * result / (Math.pow(2, genotype.length) - 1);
    }

    /**
     * Krzyżuje chromosomy poprzez krzyżowanie jednopunktowe.
     */
    public Chromosome[] crossover(Chromosome b)
    {
        int genotypeLength = getGenotype().length;
        int splitIndex = rnd.nextInt(genotypeLength + 1);

        boolean[] genotype1 = new boolean[genotypeLength];
        boolean[] genotype2 = new boolean[genotypeLength];

        System.arraycopy(getGenotype(), 0, genotype1, 0, splitIndex);
        System.arraycopy(b.getGenotype(), splitIndex, genotype1, splitIndex, genotypeLength - splitIndex);

        System.arraycopy(b.getGenotype(), 0, genotype2, 0, splitIndex);
        System.arraycopy(getGenotype(), splitIndex, genotype2, splitIndex, genotypeLength - splitIndex);

        Chromosome[] newChromosomes = new Chromosome[2];
        newChromosomes[0] = new Chromosome(genotype1);
        newChromosomes[1] = new Chromosome(genotype2);

        return newChromosomes;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Chromosome)) {
            return false;
        }
        Chromosome c = (Chromosome) o;
        return genotype == c.genotype || Arrays.equals(genotype, c.genotype);
    }
}
