package pszt.model.ga;

import java.util.Random;

import pszt.common.Range;
import pszt.mockups.IndividualMockup;

/**
 * Klasa reprezentuje osobnika.
 *
 * Osobnik składa się z dwóch chromosomów: x, y oraz wartości funkcji dopasowania.
 *
 * Wywołanie dowolnej metody klasy nie zmienia jej, co ułatwia zastosowanie wątków.
 */
public class Individual implements Comparable<Individual>
{
	private static Random		rnd	= new Random();

	private Chromosome			chromosomeX, chromosomeY;
	private double				fitness;
    private Range               range;

    /**
     * Zwraca losowego osobnika, którego fenotyp należy do podanego zakresu.
     */
	public static Individual generateRandom(final Range range)
	{
		int chromosomeSize = 24;
		boolean[] genotypeX = new boolean[chromosomeSize];
		boolean[] genotypeY = new boolean[chromosomeSize];
		for (int i = 0; i < chromosomeSize; ++i)
		{
			genotypeX[i] = rnd.nextBoolean();
			genotypeY[i] = rnd.nextBoolean();
		}
		Chromosome cx = new Chromosome(genotypeX);
		Chromosome cy = new Chromosome(genotypeY);
		return new Individual(cx, cy, range);
	}

	public double getFitness()
	{
		return fitness;
	}

    /**
     * Zwraca zmutowanego osobnika.
     * @param mutationRatio prawdopodobieństwo mutacji genu.
     */
	public Individual mutate(float mutationRatio)
	{
		Chromosome cx = chromosomeX.mutate(mutationRatio);
		Chromosome cy = chromosomeY.mutate(mutationRatio);
		return new Individual(cx, cy, range);
	}

    /**
     * Krzyżuje osobnik z podanym.
     * @return osobnik, który powstał w wyniku krzyżowania.
     */
	public Individual[] mate(Individual mate)
	{
        Chromosome[] crossedX = chromosomeX.crossover(mate.chromosomeX);
        Chromosome[] crossedY = chromosomeY.crossover(mate.chromosomeY);

        Individual child1 = new Individual(new Chromosome(crossedX[0]),
                new Chromosome(crossedY[0]), range);
        Individual child2 = new Individual(new Chromosome(crossedX[1]),
                new Chromosome(crossedY[1]), range);

        return new Individual[] {child1, child2};
	}

	public double getX()
	{
		return chromosomeX.getValue(range.getMin(), range.getMax());
	}

	public double getY()
	{
		return chromosomeY.getValue(range.getMin(), range.getMax());
	}

	private Individual(Chromosome cx, Chromosome cy, Range range)
	{
		chromosomeX = cx;
		chromosomeY = cy;
        this.range = range;
		fitness = calculateFitness();
	}

    /**
     * Oblicza wartość funkcji dopasowania.
     *
     * Liczona jest wartość funkcji, którą minimalizujemy.
     */
	private double calculateFitness()
	{
		double x = getX();
		double y = getY();
		double licz = Math.pow(Math.sin(Math.sqrt(x * x + y * y)), 2) - 0.5;
		double mian = Math.pow(1 + 0.001 * (x * x + y * y), 2);
		return 0.5 + licz / mian;
	}

	@Override
	public int compareTo(Individual o)
	{
		if (fitness < o.fitness)
		{
			return -1;
		}
		else if (fitness > o.fitness)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof Individual))
		{
			return false;
		}
		Individual i = (Individual) o;
		return chromosomeX == i.chromosomeX && chromosomeY == i.chromosomeY
				&& fitness == i.fitness;
	}

	public IndividualMockup getMockup()
	{
		return new IndividualMockup((float) getX(), (float) getY(), (float) getFitness());
	}
}
