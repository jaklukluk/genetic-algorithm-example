package pszt.model.ga;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;
import pszt.mockups.IndividualMockup;
import pszt.mockups.PopulationMockup;
import pszt.common.Range;

/**
 * Klasa opisująca populację i umożliwiająca przeprowadzenie jej ewolucji.
 *
 * Nowa populacja jest tworzona w następujący sposób:
 * - część najlepszych osobników jest zachowywana bez zmiany,
 * - nowe osobniki są tworzone korzystając z operatora krzyżowania i mutacji.
 *
 * Nowa populacja zastępuje w całości starą (oprócz części najlepszych osobników),
 * osobniki które wchodzą do nowej populacji nie są wybierane z dwóch populacji,
 * starej i nowej.
 */
public class Population
{
	private static Random	rnd	= new Random();

    /** Tablica zawierająca aktualną populację. */
	private Individual[]	popArr;
    /** Prawdopodobieństwo skrzyżowania dwóch osobników. */
	private final float		crossoverRatio;
    /** Współczynnik elitarności, czyli jaka część najlepszych osobników
     * ma przejść do kolejnej populacji. */
	private final float		elitismRatio;
    /** Prawdopodobieństwo mutacji genu osobnika. */
	private final float		mutationRatio;
    /** Zakres poszukiwania. */
	private final Range		range;
    /** Liczba wątków. */
    private int             workersNumber;

    /**
     * Tworzy losową populację o podanych parametrach.
     */
	public Population(final int size, final float crossoverRatio,
			final float elitismRatio, final float mutationRatio, final Range range)
	{
		this.crossoverRatio = crossoverRatio;
		this.elitismRatio = elitismRatio;
		this.mutationRatio = mutationRatio;
		this.range = range;

		popArr = new Individual[size];
		for (int i = 0; i < size; ++i)
		{
			popArr[i] = Individual.generateRandom(range);
		}

        workersNumber = 3;
	}

    /**
     * Przeprowadza ewolucję na populacji, czyli wyznacza kolejną populację.
     */
	public void evolve()
	{
		Individual[] buffer = new Individual[popArr.length];

        // Rozdzielenie pracy między wątki, które wyznaczą cześć populacji
        // krzyżując i mutując osobniki.
        int begin = Math.round(popArr.length * elitismRatio);
        int size = popArr.length - begin;
        int step = size / workersNumber;
        Thread workerThreads[] = new Thread[workersNumber];
        for (int i = 0; i < workersNumber; ++i)
        {
            int start = begin + step * i;
            int end = (i == workersNumber - 1) ? popArr.length : begin + step * (i + 1);
            Worker worker = new Worker(buffer, start, end);
            workerThreads[i] = new Thread(worker);
            workerThreads[i].start();
        }

		// Przekopiuj cześć najlepszych osobników.
		System.arraycopy(popArr, 0, buffer, 0, begin);

        // Poczekaj na skończenie pracy przez wątki.
        for (Thread workerThread : workerThreads)
        {
            try
            {
                workerThread.join();
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

		Arrays.sort(buffer);
		popArr = buffer;
	}

    /**
     * Zwraca kopię populacji.
     */
	public Individual[] getPopulation()
	{
		Individual[] arr = new Individual[popArr.length];
		System.arraycopy(popArr, 0, arr, 0, popArr.length);
		return arr;
	}

	public double getMutationRatio()
	{
		return mutationRatio;
	}

	public double getElitismRatio()
	{
		return elitismRatio;
	}

	public double getCrossoverRatio()
	{
		return crossoverRatio;
	}

	public int getPopSize()
	{
		return popArr.length;
	}

	public Range getRange()
	{
		return range;
	}

    public void setWorkersNumber(int workersNumber)
    {
        if (workersNumber > 0)
        {
            this.workersNumber = workersNumber;
        }
    }

	public PopulationMockup getMockup()
	{
		LinkedList<IndividualMockup> currentPop = new LinkedList<>();
		for (Individual individual : popArr)
		{
			currentPop.add(individual.getMockup());
		}
		return new PopulationMockup(currentPop);
	}

	/**
	 * Wybiera losowo osobniki z populacji.
	 * 
	 * @param n liczba osobników do wybrania
	 * @return wybrane osobniki
	 */
	private Individual[] selectParents(int n)
	{
		Individual[] parents = new Individual[n];
		for (int i = 0; i < n && i < popArr.length; ++i)
		{
			parents[i] = popArr[rnd.nextInt(popArr.length)];
		}
		return parents;
	}

    /**
     * Wątek wypełniający część przekazanej tablicy nowymi osobnikami
     * otrzymanymi drogą krzyżowania i mutacji.
     */
    private class Worker implements Runnable
    {
        /** Referencja do tablicy z nową populacją. */
        private Individual[] buffer;
        /** Od jakiego indeksu wypełniać tablicę. */
        private final int begin;
        /** Do jakiego indeksu wyłącznie wypełniać tablicę. */
        private final int end;

        public Worker(Individual[] buffer, int begin, int end)
        {
            this.buffer = buffer;
            this.begin = begin;
            this.end = end;
        }

        @Override
        public void run()
        {
            for (int index = begin; index < end; ++index)
            {
                if (rnd.nextFloat() < crossoverRatio)
                {
                    // Wybierz losowo dwóch rodziców i poddaj krzyżowaniu.
                    Individual[] parents = selectParents(2);
                    Individual[] children = parents[0].mate(parents[1]);
                    // Dzieci być może także trzeba poddać mutacji.
                    buffer[index++] = children[0].mutate(mutationRatio);
                    if (index < end)
                    {
                        buffer[index] = children[1].mutate(mutationRatio);
                    }
                }
                else
                {
                    buffer[index] = popArr[index].mutate(mutationRatio);
                }
            }
        }
    }
}
