package pszt.common;

/**
 * Reprezentuje przedział (min, max) w dziedzinie liczb rzeczywistych (double).
 * 
 * @author Jakub Szuppe
 *
 */
public class Range {

    private double min, max;

    public Range(double min, double max) {
        if (min > max) {
            double tmp = min;
            min = max;
            max = tmp;
        }
        this.min = min;
        this.max = max;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }
}
