package pszt.common;

/**
 * Punkt w przestrzeni trójwymiarowej.
 * 
 * x - położenie na osi X y - położenie na osi Y v - położenie na osi V (wartość)
 * 
 * @author Lukasz Wojcicki
 */
public class PointXYV
{

	/** Położeni na osi X */
	private float	x;
	/** Położeni na osi Y */
	private float	y;
	/** Położeni na osi V; wartość (value) */
	private float	v;

	public PointXYV(float x, float y, float v)
	{
		this.x = x;
		this.y = y;
		this.v = v;
	}

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public float getV()
	{
		return v;
	}

	public void setV(float v)
	{
		this.v = v;
	}
}
