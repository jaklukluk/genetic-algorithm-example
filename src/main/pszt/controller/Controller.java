/**
 * 
 */
package pszt.controller;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

import pszt.events.*;
import pszt.mockups.*;
import pszt.model.Model;
import pszt.model.ModelState;
import pszt.view.View;

/**
 * Kontroler - trzon aplikacji, klasa zarządzająca działaniem programu (patrz wzorzec MVC). 
 * 
 * Przyjmuje zdarzenia powodowane przez użytkownika od widoku i odpowiednio na nie reaguje,
 * wywołując stosowne działania w modelu oraz odświeżając widok.
 * 
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class Controller
{
	/** Model. */
	private final Model											model;
	/** Widok. Odpowiada za wyświetlanie stanu modelu. */
	private final View											view;
	/** Kolejka blokująca zdarzeń aplikacji {@link AppEvent} */
	private final BlockingQueue<AppEvent>						eventsBlockingQueue;
	/** Mapa strategii. Każdemu zdarzeniu aplikacji odpowiada strategia. */
	private final Map<Class<? extends AppEvent>, AppStrategy>	strategyMap;

	/**
	 * Konstruktor. Tworzy obiekt typu {@link Controller}, który jest trzonem aplikacji i
	 * zarządza podanym modelem oraz widokiem. Aplikacja nie startuje do czasu wykonania
	 * metody {@link #init()}.
	 * 
	 * @param model - model, nad którym odbywa się kontrola.
	 * @param view - widok, do który będzie wyświetlał stan modelu.
	 * @param eventsBlockingQueue - kolejka blokująca zdarzeń aplikacji.
	 */
	public Controller(final Model model, final View view,
			final BlockingQueue<AppEvent> eventsBlockingQueue)
	{
		this.model = model;
		this.view = view;
		this.eventsBlockingQueue = eventsBlockingQueue;
		this.strategyMap = new HashMap<Class<? extends AppEvent>, AppStrategy>();
		initStrategyMap();
	}

	/**
	 * Uruchamia aplikacje.
	 */
	public void init()
	{
		view.init();

		// Główną pętla aplikacji.
		while (true)
		{
			AppEvent nextAppEvent = null;

			try
			{
				// Jeżeli nie ma zdarzenia, to usypia.
				nextAppEvent = eventsBlockingQueue.take();
			} catch (InterruptedException e)
			{
				// Nie powinno się zdarzyć.
				throw new RuntimeException();
			}

			strategyMap.get(nextAppEvent.getClass()).process(nextAppEvent);
		}
	}

	/**
	 * Wypełnia {@link #strategyMap} strategiami, które odpowiadają wszystkim zdarzeniom,
	 * występującym w aplikacji.
	 */
	private void initStrategyMap()
	{
		strategyMap.clear();
		strategyMap.put(StartEvolutionEvent.class, new StartEvolutionStrategy());
		strategyMap.put(PauseEvolutionEvent.class, new PauseEvolutionStrategy());
		strategyMap.put(StopEvolutionEvent.class, new StopEvolutionStrategy());
		strategyMap.put(EvolutionEndedEvent.class, new EvolutionEndedStrategy());
		strategyMap.put(NewPopulationEvent.class, new NewPopulationStrategy());
        strategyMap.put(ShowChosenPopulationEvent.class, new ShowChosenPopulationStrategy());
	}

	/**
	 * Abstrakcyjna klasa zdarzenia aplikacji. Wszystkie zdarzenia aplikacji powinny
	 * rozszerzać tą klasę.
	 * 
	 * @author Jakub Szuppe <j.szuppe at gmail.com>
	 */
	private abstract class AppStrategy
	{
		public abstract void process(AppEvent event);
	}

	/**
	 * Strategia odpowiadająca za rozpoczęcie ewolucji populacji.
	 * 
	 * @author Jakub Szuppe <j.szuppe at gmail.com>
	 */
	class StartEvolutionStrategy extends AppStrategy
	{
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.szuppe.jakub.controller.Controller.AppStrategy#process(com.szuppe.jakub
		 * .events.AppEvent)
		 */
		public void process(AppEvent event)
		{
			if (!model.hasEvolutionStarted())
			{
				// Zbieramy dane przekazane przez widok.
				StartEvolutionEvent e = (StartEvolutionEvent) event;
				EvolutionParametersMockup evoParams = e.getEvolutionParametersMockup();
				SettingsMockup settingsMockup = e.getSettingsMockup();
				
				// Interpretacja danych, uruchomienie ewolucji.
				
				view.setEnableStartButton(false);				
				model.enableCharts(settingsMockup.areChartsEnabled());
				// Uruchamiamy ewolucje
				model.startEvolution(evoParams);
				view.setEnablePauseStopButtons(true);
				
				RangeMockup rangeMockup = settingsMockup.getRangeMockup();
				view.setRangeMockup(rangeMockup);
				
				// Wyczyszczenie wykresów i podglądu populacji
				
				view.clearCharts();
                view.clearPopulationPreview();
                view.clearPopulationPreviewNumber();
			}

		}
	}

	/**
	 * Strategia odpowiadająca za zarządzanie pauzowaniem procesu ewolucji.
	 * 
	 * Odpowiednio pauzuje, lub wznawia proces ewolucji.
	 * 
	 * @author Jakub Szuppe <j.szuppe at gmail.com>
	 */
	class PauseEvolutionStrategy extends AppStrategy
	{
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.szuppe.jakub.controller.Controller.AppStrategy#process(com.szuppe.jakub
		 * .events.AppEvent)
		 */
		public void process(AppEvent event)
		{
			// Ewolucja biegnie
			if (model.evolutionRuns())
			{
				// Więc ją pauzujemy.
				model.pauseEvolution();
				PopulationMockup populationMockup = model.getLastPopulation();
				
				// Rysowanie podglądu populacji oraz wykresów.
                view.displayPopulationPreview(populationMockup);
                view.displayPopulationPreviewNumber(model.getLastPopulationIndex());                
                if(!model.areChartsEnable())
                {
                	// Jeżeli wykresy nie są rysowane na bieżąco, to je rysujemy teraz.
                	LinkedList<IndividualMockup> bestInds = model.getBestIndividuals(); 
                	view.drawCharts(populationMockup, bestInds);
                }
			}
			// Ewolucja nie biegnie, ale jest rozpoczęta
			else if (model.hasEvolutionStarted())
			{
				// Wznawiamy ją.
				model.unpauseEvolution();
			}
		}
	}

	/**
	 * Strategia odpowiadająca za obsługę ręcznego anulowania dalszej
	 * ewolucji.
	 * 
	 * @author Jakub Szuppe <j.szuppe at gmail.com>
	 */
	class StopEvolutionStrategy extends AppStrategy
	{
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.szuppe.jakub.controller.Controller.AppStrategy#process(com.szuppe.jakub
		 * .events.AppEvent)
		 */
		public void process(AppEvent event)
		{
			// Zatrzymuje ewolucje, jeżeli była rozpoczęta
			if (model.hasEvolutionStarted())
			{
				view.setEnablePauseStopButtons(false);
				model.stopEvolution();
				PopulationMockup populationMockup = model.getLastPopulation();
                view.displayPopulationPreview(populationMockup);
                view.displayPopulationPreviewNumber(model.getLastPopulationIndex());
                if(!model.areChartsEnable())
                {
                	// Jeżeli wykresy nie są rysowane na bieżąco, to je rysujemy teraz.
                	LinkedList<IndividualMockup> bestInds = model.getBestIndividuals(); 
                	view.drawCharts(populationMockup, bestInds);
                }
				view.setEnableStartButton(true);
			}
		}
	}

	/**
	 * Strategia odpowiadająca za sytuacje, kiedy ewolucja dobiegła końca.
	 * 
	 * @author Jakub Szuppe <j.szuppe at gmail.com>
	 */
	class EvolutionEndedStrategy extends AppStrategy
	{
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.szuppe.jakub.controller.Controller.AppStrategy#process(com.szuppe.jakub
		 * .events.AppEvent)
		 */
		public void process(AppEvent event)
		{
			if (model.hasEvolutionStarted())
			{
				view.setEnablePauseStopButtons(false);
				// Ustawiamy koniec ewolucji.
				model.setModelState(ModelState.EVOLUTION_ENDED);	
				
				// Rysujemy wykresy i podgląd.
				PopulationMockup populationMockup = model.getLastPopulation();				
                view.displayPopulationPreview(populationMockup);
                view.displayPopulationPreviewNumber(model.getLastPopulationIndex());
                if(!model.areChartsEnable())
                {
                	// Jeżeli wykresy nie są rysowane na bieżąco, to je rysujemy teraz.
                	LinkedList<IndividualMockup> bestInds = model.getBestIndividuals(); 
                	view.drawCharts(populationMockup, bestInds);
                }
				view.setEnableStartButton(true);
			}
		}
	}

	/**
	 * Obsługa powstania nowej populacji w procesie ewolucji. 
	 * 
	 * @author Jakub Szuppe
	 */
	class NewPopulationStrategy extends AppStrategy
	{

		@Override
		public void process(AppEvent event)
		{
			NewPopulationEvent e = (NewPopulationEvent) event;
			PopulationMockup populationMockup = e.getPopulationMockup();
			model.addPopulation(populationMockup);
			LinkedList<IndividualMockup> bestInds = model.getBestIndividuals();
			// Rysowanie wykresów, jeżeli ustawione jest stałe rysowanie wykresów.
			if (model.evolutionRuns() && model.areChartsEnable()) 
			{
				view.drawCharts(populationMockup, bestInds);
			}
		}
	}

	/**
	 * Reaguje na wybranie populacji na wykresie trajektorii.
	 * 
	 * Rysuje wykresy dla wybranej populacji, jeżeli obecnie
	 * nie biegnie ewolucja.
	 * 
	 * @author Jakub Szuppe
	 */
    class ShowChosenPopulationStrategy extends AppStrategy
    {
        public void process(AppEvent event)
        {
            ShowChosenPopulationEvent e = (ShowChosenPopulationEvent) event;
            int populationNumber = e.getPopulationNumber();

            if(model.evolutionRuns()) return;

            // Rysowanie podglądu i wykresów dla wybranej populacji.
            view.displayPopulationPreview(model.getOldPopulation(populationNumber));
            view.drawCharts(model.getOldPopulation(populationNumber), model.getBestIndividuals());
            view.displayPopulationPreviewNumber(populationNumber);
        }
    }
}
