/**
 * 
 */
package pszt.view;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import javax.swing.SwingUtilities;
import org.jfree.chart.ChartPanel;
import pszt.events.AppEvent;
import pszt.mockups.IndividualMockup;
import pszt.mockups.PopulationMockup;
import pszt.mockups.RangeMockup;
import pszt.view.charts.LineChartFactory;
import pszt.view.charts.ScatterColouredChartFactory;

/**
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class View implements Runnable
{

	/** Główne okno. */
	private final MainFrame						mainFrame;
	/** Kolejka blokująca zdarzeń - umieszczamy w niej zdarzenia wykryte przez widok. */
	@SuppressWarnings("unused")
	private final BlockingQueue<AppEvent>		eventsBlockingQueue;
	/** Zakres poszukiwania. */
	private RangeMockup							rangeMockup;

	public View(BlockingQueue<AppEvent> eventsBlockingQueue)
	{
		this.eventsBlockingQueue = eventsBlockingQueue;
		this.mainFrame = new MainFrame(eventsBlockingQueue);
		this.rangeMockup = null;
	}

	/**
	 * Uwidacznia główne okno aplikacji.
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		mainFrame.setVisible(true);
	}

	/**
	 * Uruchamia widok.
	 */
	public void init()
	{
		SwingUtilities.invokeLater(this);
	}

	public void drawCharts(final PopulationMockup populationMockup,
			final LinkedList<IndividualMockup> bestIndividuals)
	{
		final ChartPanel chartPanel = ScatterColouredChartFactory.chart(
				populationMockup.getPopulation(), rangeMockup);
		final ChartPanel chartLowPanel = LineChartFactory.chart(bestIndividuals);

		try
		{
			SwingUtilities.invokeAndWait(new Runnable()
			{
				@Override
				public void run()
				{
					mainFrame.setUpperChartPanel(chartPanel);
					mainFrame.setLowerChartPanel(chartLowPanel);
				}
			});
		} catch (InvocationTargetException | InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	
	public void clearCharts()
	{
		final ChartPanel chartPanel = ScatterColouredChartFactory.emptyChart(rangeMockup);
		final ChartPanel chartLowPanel = LineChartFactory.emptyChart();
		
		try
        {
            SwingUtilities.invokeAndWait(new Runnable()
            {
                public void run()
                {
                          	
                	mainFrame.setUpperChartPanel(chartPanel);
					mainFrame.setLowerChartPanel(chartLowPanel);
                }
            });
        }
        catch (InvocationTargetException | InterruptedException e)
        {
            e.printStackTrace();
        }		
	}

    public void displayPopulationPreview(final PopulationMockup populationMockup)
    {
        try
        {
            SwingUtilities.invokeAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    PopulationPreview populationPreview = mainFrame.getPopulationPreview();
                    populationPreview.displayPopulation(populationMockup);
                }
            });
        } catch (InvocationTargetException | InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public void displayPopulationPreviewNumber(final int number)
    {
        try
        {
            SwingUtilities.invokeAndWait(new Runnable()
            {
                public void run()
                {
                    mainFrame.setPopulationPreviewNumber(number);
                }
            });
        }
        catch (InvocationTargetException | InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public void clearPopulationPreviewNumber()
    {
        try
        {
            SwingUtilities.invokeAndWait(new Runnable()
            {
                public void run()
                {
                    mainFrame.clearPopulationPreviewNumber();
                }
            });
        }
        catch (InvocationTargetException | InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public void clearPopulationPreview()
    {
        try
        {
            SwingUtilities.invokeAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    PopulationPreview populationPreview = mainFrame.getPopulationPreview();
                    populationPreview.clear();
                }
            });
        } catch (InvocationTargetException | InterruptedException e)
        {
            e.printStackTrace();
        }
    }

	public void disableAllButtons()
	{
		try
		{
			SwingUtilities.invokeAndWait(new Runnable()
			{
				@Override
				public void run()
				{
					mainFrame.setEnabledPauseButton(false);
					mainFrame.setEnabledStartButton(false);
					mainFrame.setEnabledStopButton(false);
				}
			});
		} catch (InvocationTargetException | InterruptedException e)
		{
			e.printStackTrace();
		}

	}

	public void setEnableStartButton(final boolean enabled)
	{
		try
		{

			SwingUtilities.invokeAndWait(new Runnable()
			{
				@Override
				public void run()
				{
					mainFrame.setEnabledStartButton(enabled);
				}
			});
		} catch (InvocationTargetException | InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	public void setEnablePauseStopButtons(final boolean enabled)
	{
		try
		{

			SwingUtilities.invokeAndWait(new Runnable()
			{
				@Override
				public void run()
				{
					mainFrame.setEnabledStopButton(enabled);
					mainFrame.setEnabledPauseButton(enabled);
				}
			});
		} catch (InvocationTargetException | InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	public void setRangeMockup(final RangeMockup rangeMockup)
	{
		this.rangeMockup = rangeMockup;
	}


}
