/**
 * 
 */
package pszt.view;

import javax.swing.*;
import java.awt.*;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import pszt.events.*;
import pszt.mockups.EvolutionParametersMockup;
import pszt.mockups.PopulationParametersMockup;
import pszt.mockups.RangeMockup;
import pszt.mockups.SettingsMockup;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.concurrent.BlockingQueue;

/**
 * Główne i jedyne okno aplikacji.
 * 
 * @author Lukasz Wojcicki <wojcickiluk at gmail.com>
 * 
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame implements ActionListener, ChartMouseListener
{
	// final JFrame frame;
	final JButton					startButton;
	final JButton					stopButton;
	final JButton					pauseButton;
	final JTextField				crossoverRatioField;
	final JTextField				mutationRatioField;
	final JTextField				elitismRatioField;
	final JTextField				populationSizeField;
    final JTextField                maxItrField;
    final JLabel                    crossoverRatioLabel;
	final JLabel					mutationRatioLabel;
	final JLabel					elitismRatioLabel;
	final JLabel					populationLabel;
    final JLabel                    minRangeLabel;
    final JLabel                    maxRangeLabel;
    final JLabel                    maxItrLabel;
    final JLabel                    visualisationLabel;
    final JLabel                    fitnessEpsilonLabel;
    final JLabel                    refreshTimeLabel;
    final JLabel                    populationPreviewLabel;
    final JRadioButton              visualisationYesRadioButton;
    final JRadioButton              visualisationNoRadioButton;
    final JSlider                   fitnessSlider;
    final JSlider                   refreshTimeSlider;
    final JSlider                   minRangeSlider;
    final JSlider                   maxRangeSlider;
	final BlockingQueue<AppEvent>	eventBlockingQueue;
    final PopulationPreview         populationPreview;
	final JPanel					upperChartPanel;
	final JPanel					lowerChartPanel;
    final String                    populationPreviewBaseText;

	MainFrame(BlockingQueue<AppEvent> eventBlockingQueue)
	{
        populationPreviewBaseText = new String("Podgląd populacji");

		this.eventBlockingQueue = eventBlockingQueue;

		startButton = new JButton("Start");
		stopButton = new JButton("Stop");
		pauseButton = new JButton("Pauza");

		crossoverRatioField = new JTextField("0.9");
		mutationRatioField = new JTextField("0.05");
		elitismRatioField = new JTextField("0.1");
		populationSizeField = new JTextField("1000");
        maxItrField = new JTextField("1000");


		crossoverRatioLabel = new JLabel("Wsp. krzyżowania");
		mutationRatioLabel = new JLabel("Wsp. mutacji");
		elitismRatioLabel = new JLabel("Wsp. elitarności");
		populationLabel = new JLabel("Rozmiar populacji");
        minRangeLabel = new JLabel("Zakres min");
        maxRangeLabel = new JLabel("Zakres max");
        maxItrLabel = new JLabel("Liczba iteracji");
        visualisationLabel = new JLabel("Wizualizacja");
        fitnessEpsilonLabel = new JLabel("Rząd obliczeń");
        refreshTimeLabel = new JLabel("Okres powtarzania (ms)");

        visualisationYesRadioButton = new JRadioButton("tak");
        visualisationNoRadioButton = new JRadioButton("nie");
        ButtonGroup radioGroup = new ButtonGroup();
        radioGroup.add(visualisationYesRadioButton);
        radioGroup.add(visualisationNoRadioButton);
        visualisationYesRadioButton.setSelected(true);

        fitnessSlider = new JSlider(JSlider.HORIZONTAL, 2, 12, 8);
        fitnessSlider.setMinorTickSpacing(1);
        fitnessSlider.setMajorTickSpacing(2);
        fitnessSlider.setPaintTicks(true);
        fitnessSlider.setPaintLabels(true);

        refreshTimeSlider = new JSlider(JSlider.HORIZONTAL, 5, 100, 10);
        refreshTimeSlider.setMinorTickSpacing(5);
        refreshTimeSlider.setMajorTickSpacing(25);
        refreshTimeSlider.setPaintTicks(true);
        refreshTimeSlider.setPaintLabels(true);

        minRangeSlider = new JSlider(JSlider.HORIZONTAL, -21, -1, -10);
        minRangeSlider.setMinorTickSpacing(1);
        minRangeSlider.setMajorTickSpacing(5);
        minRangeSlider.setPaintTicks(true);
        minRangeSlider.setPaintLabels(true);

        maxRangeSlider = new JSlider(JSlider.HORIZONTAL, 1, 21, 10);
        maxRangeSlider.setMinorTickSpacing(1);
        maxRangeSlider.setMajorTickSpacing(5);
        maxRangeSlider.setPaintTicks(true);
        maxRangeSlider.setPaintLabels(true);

		// layout
		// interfejs do wpisywania/klikania
		JPanel userPanel = new JPanel();
		GridLayout userCommunicationLayout = new GridLayout(13, 2, 3, 3);
		userPanel.setLayout(userCommunicationLayout);
        userPanel.add(visualisationLabel);
        userPanel.add(new JLabel(""));
        userPanel.add(visualisationYesRadioButton);
        userPanel.add(visualisationNoRadioButton);
        userPanel.add(fitnessEpsilonLabel);
        userPanel.add(fitnessSlider);
        userPanel.add(maxItrLabel);
        userPanel.add(maxItrField);
		userPanel.add(populationLabel);
		userPanel.add(populationSizeField);
		userPanel.add(crossoverRatioLabel);
		userPanel.add(crossoverRatioField);
		userPanel.add(mutationRatioLabel);
		userPanel.add(mutationRatioField);
		userPanel.add(elitismRatioLabel);
		userPanel.add(elitismRatioField);
        userPanel.add(minRangeLabel);
        userPanel.add(minRangeSlider);
        userPanel.add(maxRangeLabel);
        userPanel.add(maxRangeSlider);
        userPanel.add(refreshTimeLabel);
        userPanel.add(refreshTimeSlider);
		userPanel.add(startButton);
		userPanel.add(stopButton);
		userPanel.add(pauseButton);
		userPanel.add(new JLabel());
		
		// layout glownego okna
		GridBagLayout mainWindowLayout = new GridBagLayout();

		GridBagConstraints userPanelConstraints = new GridBagConstraints();
		userPanelConstraints.fill = GridBagConstraints.NONE;
		userPanelConstraints.weightx = 0;
		userPanelConstraints.weighty = 1;
		userPanelConstraints.gridx = 0;
		userPanelConstraints.gridy = 0;
		userPanelConstraints.gridheight = 2;

		//gorny wykres
		GridBagConstraints upperPlotConstraints = new GridBagConstraints();
		upperPlotConstraints.fill = GridBagConstraints.BOTH;
		upperPlotConstraints.weightx = 3;
		upperPlotConstraints.weighty = 3;
		upperPlotConstraints.gridx = 1;
		upperPlotConstraints.gridy = 0;
		upperChartPanel = new JPanel();
		upperChartPanel.setLayout(new GridLayout(1, 1));
		upperChartPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		mainWindowLayout.setConstraints(upperChartPanel, upperPlotConstraints);
		add(upperChartPanel);

        //dolny wykres
		GridBagConstraints lowerPlotConstraints = new GridBagConstraints();
		lowerPlotConstraints.fill = GridBagConstraints.BOTH;
		lowerPlotConstraints.weightx = 3;
		lowerPlotConstraints.weighty = 1;
		lowerPlotConstraints.gridx = 1;
		lowerPlotConstraints.gridy = 1;
		lowerChartPanel = new JPanel();
        lowerChartPanel.setLayout(new GridLayout(1, 1));
        lowerChartPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		mainWindowLayout.setConstraints(lowerChartPanel, lowerPlotConstraints);
		add(lowerChartPanel);

        //tabela osobnikow populacji
        populationPreviewLabel = new JLabel(populationPreviewBaseText);
		populationPreview = new PopulationPreview();
		JScrollPane scrollPane = new JScrollPane(populationPreview.getTable(),
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        JPanel populationPreviewPanel = new JPanel();
        BoxLayout populationPreviewLayout =
                new BoxLayout(populationPreviewPanel, BoxLayout.Y_AXIS);
        populationPreviewPanel.setLayout(populationPreviewLayout);
        populationPreviewPanel.add(populationPreviewLabel);
        populationPreviewPanel.add(scrollPane);

		GridBagConstraints populationPreviewConstraints = new GridBagConstraints();
		populationPreviewConstraints.fill = GridBagConstraints.VERTICAL;
		populationPreviewConstraints.weightx = 0;
		populationPreviewConstraints.weighty = 1;
		populationPreviewConstraints.gridx = 2;
		populationPreviewConstraints.gridy = 0;
		populationPreviewConstraints.gridheight = 2;
        populationPreviewPanel.setPreferredSize(new Dimension(400, 0));
        populationPreviewPanel.setMinimumSize(new Dimension(400, 0));

		mainWindowLayout.setConstraints(userPanel, userPanelConstraints);
		mainWindowLayout.setConstraints(populationPreviewPanel, populationPreviewConstraints);

		startButton.addActionListener(this);
		pauseButton.addActionListener(this);
		stopButton.addActionListener(this);

		setLayout(mainWindowLayout);
		add(userPanel);
		add(populationPreviewPanel);
		pack();
		setTitle("PsztMin");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(new Dimension(1200, 768));
		setVisible(true);
	}

	public void actionPerformed(ActionEvent e)
	{
		JButton button = (JButton) e.getSource();

		try
		{
			if (button == startButton)
			{
				float crossoverRatio = Float.parseFloat(crossoverRatioField.getText());
				float mutationRatio = Float.parseFloat(mutationRatioField.getText());
				float elitismRatio = Float.parseFloat(elitismRatioField.getText());
				int populationSize = Integer.parseInt(populationSizeField.getText());
                int minRange = minRangeSlider.getValue();
                int maxRange = maxRangeSlider.getValue();
                int refreshTime = refreshTimeSlider.getValue();

				PopulationParametersMockup populationParams = new PopulationParametersMockup(
						populationSize, crossoverRatio, elitismRatio, mutationRatio,
                        (double) minRange, (double) maxRange);
				
				int maxItr = Integer.parseInt(maxItrField.getText());
				double sufficientFitness =  Math.pow(10, (double)(-fitnessSlider.getValue()));

                populationPreview.setPrecision(fitnessSlider.getValue());

				EvolutionParametersMockup evoParams = new EvolutionParametersMockup(
						maxItr, sufficientFitness, refreshTime, populationParams);
				
				SettingsMockup settingsMockup = new SettingsMockup(visualisationYesRadioButton.isSelected(),
						new RangeMockup((double) minRange, (double) maxRange));

				eventBlockingQueue.offer(new StartEvolutionEvent(evoParams, settingsMockup));

			}
			else if (button == pauseButton)
			{
				eventBlockingQueue.offer(new PauseEvolutionEvent());
			}
			else if (button == stopButton)
			{
				eventBlockingQueue.offer(new StopEvolutionEvent());
			}
		} catch (NumberFormatException err)
		{
			System.err.println(err.getCause());
		}
	}

    /**
     * Obsluga klikniec mysza na trajektorii.
     *
     * @param e
     */
    public void chartMouseClicked(ChartMouseEvent e)
    {
        MouseEvent me = e.getTrigger();
        if(me.getClickCount() != 2) return;

        JFreeChart chart = e.getChart();
        int populationNumber = (int) chart.getXYPlot().getDomainCrosshairValue();

        eventBlockingQueue.offer(new ShowChosenPopulationEvent(populationNumber));
    }

    /**
     * Implementacja interfejsu ChartMouseListener. Zdefiniowanie metody
     * wymagane przez potrzebe obslugi klikniec mysza.
     *
     * @param e
     */
    public void chartMouseMoved(ChartMouseEvent e)
    {
        //nie wykorzystywane
    }

    public void setPopulationPreviewNumber(int number)
    {
        populationPreviewLabel.setText(populationPreviewBaseText + ": " + number);
    }

    public void clearPopulationPreviewNumber()
    {
        populationPreviewLabel.setText(populationPreviewBaseText);
    }

	public void setEnabledStartButton(final boolean enabled)
	{
		startButton.setEnabled(enabled);
	}

	public void setEnabledStopButton(final boolean enabled)
	{
		stopButton.setEnabled(enabled);
	}

	public void setEnabledPauseButton(final boolean enabled)
	{
		pauseButton.setEnabled(enabled);
	}

	public void setUpperChartPanel(ChartPanel chart)
	{
		upperChartPanel.removeAll();
		upperChartPanel.add(chart);
		upperChartPanel.validate();
		upperChartPanel.repaint();
	}

    public void setLowerChartPanel(ChartPanel chart)
    {
        chart.addChartMouseListener(this);
        lowerChartPanel.removeAll();
        lowerChartPanel.add(chart);
        lowerChartPanel.validate();
        lowerChartPanel.repaint();
    }

    public PopulationPreview getPopulationPreview()
    {
        return populationPreview;
    }

}
