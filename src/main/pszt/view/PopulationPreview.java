package pszt.view;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import pszt.mockups.IndividualMockup;
import pszt.mockups.PopulationMockup;

/**
 * Klasa opakowywujaca tabele wyswietlana w glownym oknie programu.
 */
public class PopulationPreview
{
    private final JTable table;
    private DecimalFormat decimalFormat;

    public PopulationPreview()
    {
        table = new JTable();
        clear();
        table.setDefaultRenderer(Float.class, new FloatRenderer());
        setPrecision(8);
    }

    public JTable getTable()
    {
        return table;
    }

    public void displayPopulation(final PopulationMockup populationMockup)
    {
        TableModel tableModel =
                new PopulationTableModel(populationMockup.getPopulation());
        table.setModel(tableModel);
        TableRowSorter<TableModel> sorter =
                new TableRowSorter<>(table.getModel());
        table.setRowSorter(sorter);
    }

    public void clear()
    {
        TableModel tableModel
                = new PopulationTableModel(new LinkedList<IndividualMockup>());
        table.setModel(tableModel);
        table.setRowSorter(null);
    }

    public void setPrecision(int precision)
    {
        StringBuilder sb = new StringBuilder();
        while (precision-- > 0) {
            sb.append("0");
        }
        decimalFormat = new DecimalFormat("0." + sb.toString());
    }

    @SuppressWarnings("serial")
	private class PopulationTableModel extends AbstractTableModel
    {
        private String[] columnNames = { "X", "Y", "wartość funkcji" };
        private List<IndividualMockup> population;

        public PopulationTableModel(List<IndividualMockup> population)
        {
            this.population = population;
        }

        @Override
        public int getRowCount()
        {
            return population.size();
        }

        @Override
        public int getColumnCount()
        {
            return columnNames.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex)
        {
            IndividualMockup individual = population.get(rowIndex);
            switch (columnIndex)
            {
                case 0:
                    return individual.getX();
                case 1:
                    return individual.getY();
                case 2:
                    return individual.getV();
            }
            return null;
        }

        @Override
        public String getColumnName(int column)
        {
            return columnNames[column];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex)
        {
            return getValueAt(0, columnIndex).getClass();
        }
    }

    @SuppressWarnings("serial")
	private class FloatRenderer extends DefaultTableCellRenderer
    {
        public FloatRenderer()
        {
            setHorizontalAlignment(JLabel.RIGHT);
        }

        @Override
        protected void setValue(Object value)
        {
            String s = decimalFormat.format(value);
            setText((value == null) ? "" : s);
        }
    }
}
