package pszt.view.charts;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import pszt.common.PointXYV;
import pszt.common.Range;
import pszt.mockups.RangeMockup;

import java.awt.*;
import java.util.LinkedList;

/**
 * Fabryka generujaca wykresy punktowe w ktorych kolor okreslany jest przez paramer.
 *
 * @author Lukasz Wojcicki
 */
public class ScatterColouredChartFactory
{
	/**
	 * 
	 * @param points - list punktów do narysowania.
	 * @param range - zakres osi współrzędnych X i Y
	 * @return chart - wykres X/Y
	 */
	public static ChartPanel chart(LinkedList<? extends PointXYV> points, Range range)
	{
		XYSeriesCollection seriesCollection = new XYSeriesCollection();

		int i = 0;
		for (PointXYV point : points)
		{
			XYSeries onePointSeries = new XYSeries("" + i);
			onePointSeries.add(point.getX(), point.getY());
			seriesCollection.addSeries(onePointSeries);
			i++;
		}

		JFreeChart chart = ChartFactory.createScatterPlot("Rozkład osobników", "X", "Y",
				seriesCollection, PlotOrientation.VERTICAL, false, false, false);

		XYPlot plot = (XYPlot) chart.getPlot();

		NumberAxis axisX = (NumberAxis) plot.getDomainAxis();
		axisX.setRange(range.getMin(), range.getMax());

		NumberAxis axisY = (NumberAxis) plot.getRangeAxis();
		axisY.setRange(range.getMin(), range.getMax());

		XYItemRenderer itemRenderer = plot.getRenderer();
		
		i = 0;
		for (PointXYV point : points)
		{
			float value = point.getV();
			if (value > 1) value = 1;
			if (value < 0) value = 0;
			float[] hsbColor = Color.RGBtoHSB(206, (int) (255 * value), 0, null);
			itemRenderer.setSeriesPaint(i,
					Color.getHSBColor(hsbColor[0], hsbColor[1], hsbColor[2]));
			i++;
		}

		return new ChartPanel(chart);
	}

	public static ChartPanel emptyChart(RangeMockup rangeMockup)
	{
		return chart(new LinkedList<PointXYV>(), rangeMockup);
	}
}
