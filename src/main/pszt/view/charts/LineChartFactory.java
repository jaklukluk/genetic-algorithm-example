package pszt.view.charts;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import pszt.common.PointXYV;

import java.util.LinkedList;

/**
 * Fabryka generujaca wykresy liniowe.
 *
 * @author Lukasz Wojcicki
 */
public class LineChartFactory
{
	/**
	 * 
	 * @param values - lista punktów (X,Y,V), których współrzędna V będzie rysowana na wykresie. 
	 * @return chart - wykres
	 */
	public static ChartPanel chart(LinkedList<? extends PointXYV> values)
	{
		XYSeriesCollection seriesCollection = new XYSeriesCollection();
		XYSeries series = new XYSeries("Wartości funkcji celu");
		
		int i = 0;
		for (PointXYV pointXYV : values)
		{
			series.add(i, pointXYV.getV());
			i++;
		}
		
		seriesCollection.addSeries(series);
		JFreeChart chart = ChartFactory.createXYLineChart("Trajektoria",
				"i - ta populacja", "wartość", seriesCollection,
				PlotOrientation.VERTICAL, false, false, false);

		return new ChartPanel(chart);
	}

	public static ChartPanel emptyChart()
	{
		return chart(new LinkedList<PointXYV>());
	}
}
