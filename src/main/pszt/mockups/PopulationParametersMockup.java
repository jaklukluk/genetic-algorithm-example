package pszt.mockups;

/**
 * Makieta z parametrami populacji ustawionych przez użytkownika w oknie aplikacji.
 * 
 * Przekazywana do modelu w celu rozpoczęcia procesu ewolucji, a tym samym szukania
 * minimum funkcji.
 * 
 * @author Jakub Szuppe
 * 
 */
public class PopulationParametersMockup
{
	private final Integer	populationSize;
	private final Float		crossoverRatio;
	private final Float		elitismRatio;
	private final Float		mutationRatio;
    private final Double    minRange;
    private final Double    maxRange;

    public PopulationParametersMockup(final Integer populationSize,
			final Float crossoverRatio, final Float elitismRatio,
			final Float mutationRatio,
            final Double minRange,
            final Double maxRange)
	{
		this.populationSize = populationSize;
		this.crossoverRatio = crossoverRatio;
		this.elitismRatio = elitismRatio;
		this.mutationRatio = mutationRatio;
        this.minRange = minRange;
        this.maxRange = maxRange;
	}

	public Integer getPopulationSize()
	{
		return populationSize;
	}

	public Float getCrossoverRatio()
	{
		return crossoverRatio;
	}

	public Float getElitismRatio()
	{
		return elitismRatio;
	}

	public Float getMutationRatio()
	{
		return mutationRatio;
	}

    public Double getMinRange() {
        return minRange;
    }

    public Double getMaxRange() {
        return maxRange;
    }
}
