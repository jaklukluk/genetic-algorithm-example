package pszt.mockups;

import pszt.common.Range;

/**
 * Makieta z informacją o zakresie współczynników x i y.
 * 
 * @author Jakub Szuppe
 *
 */
public class RangeMockup extends Range {

    public RangeMockup(double min, double max) {
        super(min, max);
    }
}
