package pszt.mockups;

/**
 * Makieta z ustawieniami aplikacji.
 * 
 * @author Jakub Szuppe
 *
 */
public class SettingsMockup
{
	/** Prawda, jeżeli mamy rysować wykresy; Fałsz wpp. */
	private final boolean		enableCharts;
	/** Zakres wyświetlany na wykresie X/Y. */
	private final RangeMockup	rangeMockup;

	public SettingsMockup(final boolean enableCharts, final RangeMockup rangeMockup)
	{
		this.enableCharts = enableCharts;
		this.rangeMockup = rangeMockup;
	}

	public boolean areChartsEnabled()
	{
		return enableCharts;
	}

	public RangeMockup getRangeMockup()
	{
		return rangeMockup;
	}

}
