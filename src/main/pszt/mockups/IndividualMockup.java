package pszt.mockups;

import pszt.common.PointXYV;

/**
 * Makieta z danymi osobnika.
 * 
 * Zawiera informacje o współrzędnych XY oraz uzyskanej
 * na ich podstawie wartości funkcji.
 * 
 * @author Jakub Szuppe
 *
 */
public class IndividualMockup extends PointXYV
{
	public IndividualMockup(float x, float y, float v)
	{
		super(x, y, v);
	}
}
