package pszt.mockups;

/**
 * Makieta z parametrami ewolucji.
 * 
 * @author Jakub Szuppe
 *
 */
public class EvolutionParametersMockup
{
	/** Maksymalna ilość przebiegów ewolucji */
	private final int							maxIterations;
	/** Wystarczająca odległość od zera. */
	private final double						sufficientFitness;
	/** Okres powtarzania w ms. */
	private final int							refreshTime;
	/** Parametry populacji. */
	private final PopulationParametersMockup	popParams;

	public EvolutionParametersMockup(final int maxItr, final double sufficientFitness,
			final int refreshTime, final PopulationParametersMockup popParams)
	{
		this.maxIterations = maxItr;
		this.sufficientFitness = sufficientFitness;
		this.refreshTime = refreshTime;
		this.popParams = popParams;
	}

	public int getMaxIterations()
	{
		return maxIterations;
	}

	public double getSufficientFitness()
	{
		return sufficientFitness;
	}

	public int getRefreshTime()
	{
		return refreshTime;
	}

	public PopulationParametersMockup getPopulationParameters()
	{
		return popParams;
	}

}
