package pszt.mockups;

import java.util.LinkedList;

/**
 * Makieta do przekazywania z Modelu do Widoku danych o populacjach, które powstały w
 * tracie ewolucji.
 * 
 * @author Jakub Szuppe
 * 
 */
public class PopulationMockup
{
	/* Tablica zawierająca populacje */
	private final LinkedList<IndividualMockup>	population;

	public PopulationMockup(final LinkedList<IndividualMockup> population)
	{
		this.population = population;
	}

	public LinkedList<IndividualMockup> getPopulation()
	{
		return population;
	}
	
	public IndividualMockup getBest()
	{
		return population.getFirst();
	}

}
